# Thumbnailer

Thumbnailer is a bare-bones web application service which resizes images into 100x100px thumbnails.

A PoC for an API providing a long-running-job type service (represented by image resizing), the chosen design aims to enable
relatively painless horizontal scaling of the image-resizing capacity by leveraging Apache Kafka as a job queue. 

Some "nice to haves" with Kafka, as compared to e.g. a Celery+Redis solution (which was also considered):
- All messages / events are persisted by default, which is useful for QA, troubleshooting and tests
- Other consumers can be added in the future, should a need arise, and potentially be run on the full history of events. 

The image files are stored on a Localstack instance, acting as a stand-in for Amazon S3, or some other suitable network storage (e.g. HDFS). Kafka
messages contain paths to source images and thumbnail files, not the images themselves.

## Components
The main components of the solution, and how they interact:

Client - processing request -> API 

API - image -> S3

API - processing message -> Kafka (topic: resize requests) -> Resizer component

Resizer component - thumbnail -> S3

Resizer component - finished message -> Kafka (topic: processing finished) -> Callback component

Callback componsent - update job status in redis -> optional webhook callback, if supplied


## Usage

### Prerequisites
You will need [Docker Compose](https://docs.docker.com/compose/install/)
### Installing and running
```bash
$ git clone git@gitlab.com:daniel.bjerninger/thumbnailer.git
$ cd thumbnailer
$ docker-compose build
$ docker-compose up
```
### API
Schedule generation
```bash
$ curl http://localhost:5000/thumbnails/tasks -F 'file=@/path/to/file' -F 'callback=http://myhost.com/callbackUrl'
```
(callback is optional)

..returns status 202 and a task id
```
{
  "id": "52ae5a71de3348eeb2e01a300d209598"
}
```

Query task status
```bash
$ curl http://localhost:5000/thumbnails/tasks/52ae5a71de3348eeb2e01a300d209598
```

While processing, status 200 and
```
{
  "status", "processing"
}
```

On finish, status 303 and
```
{
  "Location": "http://localhost:4572/demo-bucket/52ae5a71de3348eeb2e01a300d209598.jpg"
}
```


### Tests

Thumbnailer comes with a rudimentary set of happy path end2end tests, under e2e/. 

Run with :
```bash
$ API_HOST=localhost pytest e2e_tests.py
```

(Running the application with 'docker-compose up' will spin up a container and run the e2e tests from it as well)

### Troubleshooting

Grep on warnings and errors.

```bash
$ docker-compose up --build --abort-on-container-exit | grep "WARN\|ERROR\|CRITICAL"
```

Grep by component

```bash
$ docker-compose up --build --abort-on-container-exit | grep "api\|resizer\|callbacker"
```

Change loglevel for api/resizer/callbacker

```docker-compose.yml
    environment:
      LOG_LEVEL: DEBUG
```
## Disclaimers and future improvements

The supplied docker-compose.yml runs on Flask's built-in development server. A production-grade WSGI server, e.g. Gunicorn, is needed in production, with nginx.

The API as currently implemented is open to any client and lack any authentication or authorization facilities, as well as facilities to protect against DDOS or other attack vectors. 
Those areas as well as other hardening and resilience measures would be necessary to review and adress before a production deploy.

The uploading of the source image file (first to the API server, then to S3/localstack) could probably be handled in a more efficient way. 

In a production setup the generated thumbnails could be cached using e.g. a web proxy cache, and on the end users browser.

No cleanup of either image source files or generated thumbnails, on local storage or S3/localstack is currently implemented. Neither is the Redis list of jobs pruned.

Level of code quality in general is currently "Proof of Concept"