"""Listens for thumbnail generation job messages
on the topic defined in the $TRANSACTION_TOPIC env variable.

On consuming a message it will download the source image file from
network storage (i.e. S3) unto local disk and generate a thumbnail
using the Pillow library.

On successful generation of the thumbnail, it will upload it to
i.e. S3 and send a message
"""
from __future__ import print_function

import os
import logging
import sys
import deal
import traceback
from PIL import Image
from kafka import KafkaProducer
from json import (
    loads,
    dumps
)
from ..shared.kafka import ensure_kafka_consumer
from ..shared.loghelp import init_logging_global_defaults
from ..shared.storage import (
    upload_file,
    download_file
)

#Kafka

KAFKA_BROKER_URL = os.environ.get('KAFKA_BROKER_URL')
TOPIC_THUMBNAIL_REQUESTED = os.environ.get('TOPIC_THUMBNAIL_REQUESTED')
TOPIC_THUMBNAIL_GENERATED = os.environ.get('TOPIC_THUMBNAIL_GENERATED')

init_logging_global_defaults()

logging.info("receiver startup")
logging.info("Topic : %s" % TOPIC_THUMBNAIL_REQUESTED)

def decode(msg):
    """Decodes a (JSON) message payload
    :param msg: The message payload as a UTF-8 string
    :return: See loads documentation
    """
    retval = loads(msg.decode('utf-8'))
    logging.info("decoded message: %s of type %s" % (retval, type(retval)))
    return retval


consumer = ensure_kafka_consumer(6,
                                 TOPIC_THUMBNAIL_REQUESTED,
                                 bootstrap_servers=[KAFKA_BROKER_URL],
                                 auto_offset_reset='earliest',
                                 enable_auto_commit=True,
                                 group_id=None,
                                 value_deserializer=decode)

producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL,
                         value_serializer=lambda x:
                         dumps(x).encode('utf-8'))


def to_thumbnail_name(local_path):
    local_parent, local_file = os.path.split(local_path)
    name_fragment, ext_fragment = os.path.splitext(local_file)
    thumb_target_filename = "thumbnail_%s_100x100%s" % (name_fragment, ext_fragment)
    return thumb_target_filename


LOCAL_UPLOAD_FOLDER = os.environ.get('LOCAL_UPLOAD_FOLDER')

SIZE = 100, 100


@deal.pre(lambda record: 'location' in record.value,
          message="'location' required")
@deal.pre(lambda record: 'id' in record.value,
          message="'id' required")
@deal.pre(lambda record: 'callback' in record.value,
          message="'callback' required")
def process_message(record):
    """Process one thumbnail generation job
    """
    job = record.value

    try:
        logging.info(job)

        logging.info("downloading file from %s...", job['location'])
        local_path = download_file(job['location'])
        thumb_target_filename = "%s.jpg" % job['id']  #to_thumbnail_name(local_path)
        thumb_tmpdir_path = os.path.join(LOCAL_UPLOAD_FOLDER, thumb_target_filename)

        logging.info("done. Generating thumbnail to %s" % thumb_tmpdir_path)
        im = Image.open(local_path)
        im.thumbnail(SIZE)
        im.convert('RGB').save(thumb_tmpdir_path, "JPEG")

        logging.info("done. Uploading %s to s3..." % thumb_target_filename)
        remote_path = upload_file(open(thumb_tmpdir_path, 'rb'), thumb_target_filename, Image.MIME[im.format])

        logging.info("done. Sending processing finished message...")

        producer.send(TOPIC_THUMBNAIL_GENERATED, value={
            'location': remote_path,
            'id': job['id'],
            'callback': job['callback'],
            'status': 'success'
        })
        logging.info("done.")

    except Exception as ex:
        logging.warning("Thumbnail generation job %s failed %s. %s" %
                        (job['id'], str(ex), traceback.format_exc()))
        producer.send(TOPIC_THUMBNAIL_GENERATED, value={
            'id': job['id'],
            'status': 'failed',
            'msg': str(ex)
        })





if __name__ == '__main__':


    logging.info('Connected to Broker, listening for jobs...')
    try:
        for record in consumer:
            try:
                process_message(record)
            except Exception as ex:
                logging.exception(ex, "Exception while generating thumbnail")
    except Exception as ex:
        logging.exception(ex, "Exception while connecting to Kafka")
    logging.critical("Process exiting....")
