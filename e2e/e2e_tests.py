import requests
import os
import time

assert 'API_HOST' in os.environ
API_HOST = os.environ.get('API_HOST')
base_path = "http://%s:5000/" % API_HOST


def test_upload_happy_flow_should_give_status_202():
    target_path = "%s%s" % (base_path, "thumbnails/tasks")
    files = {'file': ('cinqueterre.jpg', open('test_assets/cinqueterre.jpg', 'rb'), 'Image/Jpeg')}
    values = {'callback': 'www.google.com'}
    response = requests.post(target_path, files=files, data=values)
    assert response.status_code == 202


def test_upload_happy_flow_should_have_content_type_json():
    target_path = "%s%s" % (base_path, "thumbnails/tasks")
    files = {'file': ('cinqueterre.jpg', open('test_assets/cinqueterre.jpg', 'rb'), 'Image/Jpeg')}
    values = {'callback': 'www.google.com'}
    response = requests.post(target_path, files=files, data=values)
    assert response.headers['Content-Type'] == "application/json"


def test_upload_happy_flow_should_return_job_id():
    target_path = "%s%s" % (base_path, "thumbnails/tasks")
    files = {'file': ('cinqueterre.jpg', open('test_assets/cinqueterre.jpg', 'rb'), 'Image/Jpeg')}
    values = {'callback': 'www.google.com'}
    response = requests.post(target_path, files=files, data=values)
    assert 'id' in response.json()

def test_query_status_should_return_job_id():
    target_path = "%s%s" % (base_path, "thumbnails/tasks")
    files = {'file': ('cinqueterre.jpg', open('test_assets/cinqueterre.jpg', 'rb'), 'Image/Jpeg')}
    values = {'callback': 'www.google.com'}
    response = requests.post(target_path, files=files, data=values)
    task_url = "%sthumbnails/tasks/%s" % (base_path, response.json()['id'])
    response = requests.get(task_url)

    assert response.status_code == 200

def test_upload_happy_flow_should_return_job_id():
    target_path = "%s%s" % (base_path, "thumbnails/tasks")
    files = {'file': ('cinqueterre.jpg', open('test_assets/cinqueterre.jpg', 'rb'), 'Image/Jpeg')}
    values = {'callback': 'www.google.com'}
    response = requests.post(target_path, files=files, data=values)
    task_url = "%sthumbnails/tasks/%s" % (base_path, response.json()['id'])
    response = requests.get(task_url)

    assert 'status' in response.json()

def test_upload_happy_flow_status_processing():
    target_path = "%s%s" % (base_path, "thumbnails/tasks")
    files = {'file': ('cinqueterre.jpg', open('test_assets/cinqueterre.jpg', 'rb'), 'Image/Jpeg')}
    values = {'callback': 'www.google.com'}
    response = requests.post(target_path, files=files, data=values)
    task_url = "%sthumbnails/tasks/%s" % (base_path, response.json()['id'])
    response = requests.get(task_url)

    assert response.json()['status'] == 'processing'

