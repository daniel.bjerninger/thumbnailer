import os
import uuid
import redis

from json import dumps
from kafka import KafkaProducer
from werkzeug.utils import secure_filename
from flask import (
    Flask,
    request,
    jsonify
)
from ..shared.loghelp import init_logging_global_defaults
from ..shared.storage import (
    upload_file
)

# Redis, holds the list of tasks

jobs = redis.Redis(host='redis', port=6379)

# Kafka

assert 'KAFKA_BROKER_URL' in os.environ
KAFKA_BROKER_URL = os.environ.get('KAFKA_BROKER_URL')
assert 'TOPIC_THUMBNAIL_REQUESTED' in os.environ
TOPIC_THUMBNAIL_REQUESTED = os.environ.get('TOPIC_THUMBNAIL_REQUESTED')
assert 'LOCAL_UPLOAD_FOLDER' in os.environ
LOCAL_UPLOAD_FOLDER = os.environ.get('LOCAL_UPLOAD_FOLDER')
assert 'THUMBNAIL_ROOT_PATH' in os.environ
THUMBNAIL_ROOT_PATH = os.environ.get('THUMBNAIL_ROOT_PATH')

ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

# init app

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = os.environ.get('MAX_UPLOAD_SIZE')


producer = KafkaProducer(bootstrap_servers=KAFKA_BROKER_URL,
                         value_serializer=lambda x:dumps(x).encode('utf-8'))


@app.before_first_request
def setup():

    init_logging_global_defaults()

@app.after_request
def set_response_headers(response):
    response.headers['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    response.headers['Pragma'] = 'no-cache'
    response.headers['Expires'] = '0'
    return response


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/ping', methods=['GET'])
def ping():
    return 200


@app.route('/thumbnails/tasks', methods=['POST'])
def thumbnails_schedule_generation():

    # check if the post request has the file part
    if 'file' not in request.files:
        app.logger.debug("Missing file parameter, returning 400")
        resp = jsonify({'message': 'No file part in the request'})
        resp.status_code = 400
        return resp

    file = request.files['file']

    if file.filename == '':
        app.logger.debug("Missing filename, returning 400")
        resp = jsonify({'message': 'No file selected for uploading'})
        resp.status_code = 400
        return resp

    if file and allowed_file(file.filename):

        job_handle = uuid.uuid4()
        target_filename = secure_filename(file.filename)
        app.logger.info("Uploading file as %s" % target_filename)
        file_remote_location = upload_file(file, target_filename, file.content_type)

        app.logger.info("done. Scheduling thumbnail job on Kafka topic %s for id %s" %
                        (TOPIC_THUMBNAIL_REQUESTED, job_handle.hex))
        producer.send(TOPIC_THUMBNAIL_REQUESTED, value={
            'location': file_remote_location,
            'id': job_handle.hex,
            'filename': file.filename,
            'callback': request.form['callback'],
            'content-type': file.content_type
        })

        app.logger.info("done. Updating status in Redis")
        jobs.set(job_handle.hex, 'processing')
        app.logger.info("Done. Sending response..")
        resp = jsonify({'id': "%s" % job_handle.hex})
        resp.status_code = 202
        return resp

    else:

        app.logger.debug("Illegal file type %s returning 400" % file.filename)
        resp = jsonify({'message': 'Allowed file types are txt, pdf, png, jpg, jpeg, gif'})
        resp.status_code = 400
        return resp


@app.route('/thumbnails/tasks/<string:task_id>', methods=['GET'])
def get_generation_task_status(task_id):

    task = jobs.get(task_id)

    if not task:
        app.logger.debug("GET task not found: %s" % task_id)
        resp = jsonify({'message': 'Task not found %s' % task_id})
        resp.status_code = 404
        return resp

    task = task.decode("utf-8")

    if task == 'success':
        resp = jsonify({'Location': "%s%s.jpg" % (THUMBNAIL_ROOT_PATH,  task_id) })
        resp.status_code = 303
        return resp

    app.logger.info(task)
    resp = jsonify({ 'status': task })
    resp.status_code = 200
    return resp



@app.route('/callback-target', methods=['POST'])
def callback_target():
    print('Got callback:')
    print(request.json)
    resp = jsonify({'status': 'ok'})
    resp.status_code = 200
    return resp
