import requests

def test_upload_happy_flow_should_give_status_200():
    files = {'file': ('cinqueterre.jpg', open('test_assets/cinqueterre.jpg', 'rb'), 'Image/Jpeg')}
    values = {'callback': 'www.google.com'}
    response = requests.post("http://localhost:5000/generateThumbnail", files=files, data=values)
    assert response.status_code == 200
    assert response.headers['Content-Type'] == "application/json"
    assert 'id' in response.json()