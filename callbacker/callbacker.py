# callbacker/callbacker.py
from __future__ import print_function

import os
import logging
import sys
import requests
import redis

from json import loads
from ..shared.kafka import ensure_kafka_consumer
from ..shared.loghelp import init_logging_global_defaults

KAFKA_BROKER_URL = os.environ.get('KAFKA_BROKER_URL')
TOPIC_THUMBNAIL_GENERATED = os.environ.get('TOPIC_THUMBNAIL_GENERATED')

jobs = redis.Redis(host='redis', port=6379)


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def decode(msg):
    retval = loads(msg.decode('utf-8'))
    print("decoded message: %s" % retval)
    return retval


if __name__ == '__main__':

    init_logging_global_defaults()

    consumer = ensure_kafka_consumer(6,
                                     TOPIC_THUMBNAIL_GENERATED,
                                     bootstrap_servers=[KAFKA_BROKER_URL],
                                     auto_offset_reset='earliest',
                                     enable_auto_commit=True,
                                     group_id="callbacker",
                                     value_deserializer=decode)
    try:
        for message in consumer:
            try:
                job = message.value
                logging.info("Generation finished %s status %s" % (job['id'], job['status']))
                jobs.set(job['id'], job['status'])  # statis -> redis
                if job['status'] == 'success' and 'callback' in job:
                    try:
                        logging.info("doing callback for url %s" % job['callback'])
                        r = requests.post(url=job['callback'], json={
                            "thumbnail": job['location']})
                    except Exception as ex:
                        logging.info("Callback for id %s to %s failed" % (job['id'], job['location']))
                logging.info('Done.')
            except Exception as ex:
                logging.warning('failed to process job %s' % job[id], ex)
    except Exception as ex:
        logging.exception(ex, "Exception while connecting to Kafka")
    eprint("finished, exiting....")
