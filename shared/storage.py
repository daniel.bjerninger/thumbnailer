import os
import boto3


assert 'S3_KEY' in os.environ
S3_KEY = os.environ.get('S3_KEY')
assert 'S3_SECRET' in os.environ
S3_SECRET = os.environ.get('S3_SECRET')
assert 'S3_LOCATION' in os.environ
S3_LOCATION = os.environ.get('S3_LOCATION')

LOCAL_UPLOAD_FOLDER = os.environ.get('LOCAL_UPLOAD_FOLDER')

s3 = boto3.client(
    "s3",
    endpoint_url="http://localstack:4572",
    aws_access_key_id=S3_KEY,
    aws_secret_access_key=S3_SECRET
)


def upload_file(file, name, content_type):
    s3.upload_fileobj(
        file,
        S3_LOCATION,
        name,
        ExtraArgs={
            "ACL": "public-read",
            "ContentType": content_type
        }
    )
    return "{}/{}".format(S3_LOCATION, name)


def download_file(url):
    path, resource = os.path.split(url)
    local_path = os.path.join(LOCAL_UPLOAD_FOLDER, resource)
    s3.download_file(path, resource, local_path)
    return local_path
