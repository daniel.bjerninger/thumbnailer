import daiquiri
import logging
import os

def init_logging_global_defaults():
    daiquiri.setup()  # sane logging defaults with timestamps etc
    logging.getLogger().setLevel(os.getenv('LOG_LEVEL', logging.INFO))
    logging.getLogger('kafka').setLevel(logging.WARNING)
    logging.getLogger('kafka.conn').setLevel(logging.CRITICAL)