import time
import logging
from kafka import KafkaConsumer, KafkaProducer
from kafka.errors import NoBrokersAvailable
import deal


@deal.pre(lambda max_retries, topic, **kwparams: max_retries > 0,
          message="max_retries must be > 0")
@deal.pre(lambda max_retries, topic, **kwparams: topic and len(topic.strip()) > 0,  # not empty
          message="Topic must be specified!")
@deal.pre(lambda max_retries, topic, **kwparams:
          'bootstrap_servers' in kwparams, message="Must supply the 'bootstrap_servers' keyword parameter!")
def ensure_kafka_consumer(max_retries,
                          topic,
                          **kwparams):
    """Wait for Kafka to become available and return a consuming client when
    it is available"""
    for x in range(max_retries):
        try:
            return KafkaConsumer(topic,
                                 **kwparams)
        except NoBrokersAvailable:
            logging.info('Waiting for Kafka broker...')
        time.sleep(5)
    logging.critical("Failed to connect to Kafka, bailing!")
    raise NoBrokersAvailable


@deal.pre(lambda max_retries, **kwparams: max_retries > 0,
          message="max_retries must be > 0")
@deal.pre(lambda max_retries, **kwparams:
          'bootstrap_servers' in kwparams, message="Must supply the 'bootstrap_servers' keyword parameter!")
def ensure_kafka_producer(max_retries, **kwparams):
    """Wait for Kafka to become available and return a producing client when
    it is available"""
    for x in range(max_retries):
        try:
            producer = KafkaProducer(**kwparams)
        except NoBrokersAvailable:
            logging.info('Waiting for Kafka broker...')
        time.sleep(5)
    logging.critical("Failed to connect to Kafka, bailing!")
    raise NoBrokersAvailable